load "$NCL_DIR/common.ncl"

fil=addfile("LabSea.EN4.2.0.f.analysis.g10.nc","r")

;only select depths between 800m and 3500m
;times between Jun 1900 and May 2016
theta=fil->temperature(5:1396,15:35,:,:)
sal=fil->salinity(5:1396,15:35,:,:)


rho=theta
ds=dimsizes(rho)
print(ds)
do i=0,ds(0)-1
  do j=0,ds(1)-1
    rho(i,j,:,:)=rho_mwjf(theta(i,j,:,:),sal(i,j,:,:),2000)
  end do
end do
sigma=rho
sigma=(/(rho-1.)*1000./)

depths = sigma&depth
LabSeaThick = sigma(:,0,:,:)
LabSeaThick=(/LabSeaThick@_FillValue/)
do i=0,ds(0)-1
  do j=0,ds(2)-1
    do k=0,ds(3)-1
      this_column=sigma(i,:,j,k)
      if any(.not.ismissing(this_column)) then       ; avoid land
        it = ind(.not.ismissing(this_column))
        if dimsizes(it).gt.2.and.isMonotonic(this_column(it)) then
          TopBottom = linint1(this_column(it),depths(it),False,(/36.82,36.97/),0)
          ;print(" "+i+", "+j+", "+k+" water_depth="+max(depths(it))+": max_sigma="+max(this_column)+", minsigma="+min(this_column)+", Top="+TopBottom(0)+", Bottom="+TopBottom(1))
          if ismissing(TopBottom(0)).ne.ismissing(TopBottom(1)) then
            if ismissing(TopBottom(0)) then
              TopBottom(0)=min(depths)
            else
              TopBottom(1)=min((/max(depths),max(depths(it))/))
            end if
          end if
          LabSeaThick(i,j,k) = (/TopBottom(1)-TopBottom(0)/)
        end if
        delete(it)
      end if
    end do
  end do
end do

LabSeaThick@plotLatLons=(/45.,65.,-70.,-40./)
roughlatlonplot(LabSeaThick(3,:,:))

clat=cos(RAD_C*LabSeaThick&lat)
AvgThick=wgt_areaave(LabSeaThick,clat,1,0)
time=LabSeaThick&time

AnnMaxThick=dim_max(onedtond(ndtooned(AvgThick),(/ds(0)/12,12/)))
Yr=dim_avg(onedtond(ndtooned(time),(/ds(0)/12,12/)))
copy_VarAtts(time,Yr)
AnnMaxThick!0="time"
AnnMaxThick&time=Yr
do i=0,(ds(0)/12)-1
  print(ut_string(Yr(i),"%Y")+": "+AnnMaxThick(i))
end do

#!/bin/bash
#This script will extract the data from core sites

declare -a Commands=("ncks -O -v temperature,temperature_observation_weights -d depth,76.5 -d lat,43. -d lon,304."
		     "ncks -O -v temperature,temperature_observation_weights -d depth,76.5 -d lat,47. -d lon,300."
		     "ncks -O -v temperature,temperature_observation_weights -d depth,235. -d lat,47. -d lon,300."
		     "ncks -O -v temperature,temperature_observation_weights -d depth,315. -d lat,50. -d lon,302."
		     "ncks -O -v temperature,temperature_observation_weights -d depth,98. -d lat,57. -d lon,332."
		     "ncks -O -v temperature,temperature_observation_weights -d depth,98. -d lat,56. -d lon,346."
		     "ncwa -O -v temperature,temperature_observation_weights -d depth,98.,235. -d lat,50.,63. -d lon,330.,335. -a depth,lat,lon")

#declare -a CORES=("RAPID-21-3K" "ENAM9606" "GeoB6007-OC-437-24GGC" "MC13-GGC14" "MC29-GGC30-Planktic" "MC29-GGC30-Benthic" "GulfStLawrence")
declare -a CORES=("MC13-GGC14" "MC29-GGC30-Planktic" "MC29-GGC30-Benthic" "GulfStLawrence" "RAPID-21-3K" "ENAM9606" "GeoB6007-OC-437-24GGC")

this_dir=`pwd`
EN4_dir="/data/geospatial_23/brierley_temp_area/EN4.1.1"
EN4_filstr="EN.4.1.1.f.analysis.g10.*.nc"

cd $EN4_dir
fils=`ls $EN4_filstr`
num_cores=${#CORES[@]}

for (( i=0; i<${num_cores}; i++ ));
do
  echo ${CORES[$i]}
  for fil in $fils;
  do
#    echo ${Commands[i]} $fil /tmp/${CORES[$i]}.$fil
    ${Commands[i]} $fil /tmp/${CORES[$i]}.$fil
  done
  ncrcat -O /tmp/${CORES[$i]}.*.nc $this_dir/${CORES[$i]}.nc
  rm /tmp/${CORES[$i]}.*.nc
done
	     
cd $this_dir


#Updated location and directions from David 
#Northeast:
#-Rapid-21-12B 57°27.09′N, 27°54.53′W, and probably would ask for T at 75m and 200m (so I could calculate Tsub separately using both option to check if it made a big difference)
#-Rapid-17-5P 61°28.90′ 19°32.16′and probably would ask for T at 75m and 200m
#-Feni drift  55_39.020N 13_59.100W and probably would ask for T at 75m and 200m
#-And then we have the core that we have benthics for, which is off N. Africa but is meant to monitor surface east subpolar. But I realized it will be best if we actually look at what EN4 gives us for the actual in situ changes for bottom water at this site: 30.85°N, 10.27°W at 899 m water depth. But maybe there isn’t much data and in which case we would have to go with some kind of average over a box in the NEATL….let me know what you can find for the 800m depth data at the actual site
 
#Northwest:
#-Emerald Basin 62o47.4 W, 43o53.1 N, at 75m and also 250m
#-MC13 is 55o50’W 43o10’N at 75m
#-And a new record I have from Gulf of St Lawrence, COR05-37: 61o30’W, 48o20’N at 409m depth
